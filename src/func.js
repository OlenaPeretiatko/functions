const getSum = (str1, str2) => {
    let regExp = /[a-zA-Z]/g;
    if (typeof (str1) !== 'string' || typeof (str2) !== 'string' || regExp.test(str1) || regExp.test(str2)) {
        return false;
    }
    let res = '';
    let dif = str1.length - str2.length;
    if (str1.length < str2.length) {
        dif = -dif;
        for (let i = 0; i < dif; i++) {
            str1 = '0' + str1;
        }
    }
    if (str1.length > str2.length) {
        for (let i = 0; i < dif; i++) {
            str2 = '0' + str2;
        }
    }
    for (let i = 0; i < str1.length; i++) {
        res += Number(str1[i]) + Number(str2[i])
    }
    return res;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let numOfPosts = 0, numOfComments = 0;
    for (let post of listOfPosts) {
        if (post.author === authorName) {
            numOfPosts += 1;
        }
        if (post.comments !== undefined) {
            for (let comment of post.comments) {
                if (comment.author === authorName) {
                    numOfComments += 1;
                }
            }
        }
    }
    return `Post:${numOfPosts},comments:${numOfComments}`
};

const tickets = (people) => {
    let rest = [], res = true;
    for (let el of people) {
        el = Number(el)
        rest.push(Number(el));
        if (el === 25) {
            res *= true;
        } else {
            while (el !== 25) {
                if (rest.includes(el / 2)) {
                    rest.splice(rest.indexOf(el / 2), 1)
                    el /= 2;
                } else if (rest.includes(25)) {
                    rest.splice(rest.indexOf(25), 1)
                    el -= 25;
                } else {
                    res *= false;
                    break;
                }
            }
        }
    }
    return (res) ? "YES" : "NO";
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
